import os

def addDescription(fileName, newfileName):

    datafile = open(fileName, 'r')
    newfile = open(newfileName, 'w')

    for line in datafile:
        writed = False
        if "name" in line:
            name = line[13:-3]
        if "brand" in line:
            brand = line[14:-2]
            line = line + "\t\"description\" : \"" + name + " " + brand + ",\n"
            newfile.write(line)
            writed = True
        if (writed == False):
            newfile.write(line)

def addISODate(fileName, newfileName):

    datafile = open(newfileName, 'r')
    newfile = open(fileName, 'w')

    for line in datafile:
        writed = False
        if "expiredTime" in line:
            firstPart = line[0:19]
            secondPart = line[19:-2]
            line = firstPart + "{\"$date\":" + secondPart + "Z\"}"
            newfile.write(line)
            writed = True
        if (writed == False):
            newfile.write(line)


if __name__ == "__main__":

    fileName = "product.json"
    newfileName = "temp" + fileName


    addDescription(fileName, newfileName)
    addISODate(fileName, newfileName)

    os.remove(newfileName)